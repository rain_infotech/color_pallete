library colorspalette;

import 'package:colors_palette/colorList.dart';
import 'package:flutter/material.dart';

enum colorStyle {
  STYLE_NORMAL,
  STYLE_GRID_SQUARE,
  STYLE_ROUND,
  STYLE_SQUARE,
  STYLE_GRID_ROUND
}

class ColorsPalette extends StatelessWidget {
  final GestureTapCallback onPressed;

  final Function(Color) onColorChange;

  final colorStyle colorPaletteStyle;

  double height;

  double width;

  int crossAxisCount;

  Axis axis;

  double shapeHeight;

  double shapeWidth;

  ColorsPalette(
      {this.onPressed,
      this.onColorChange,
      this.colorPaletteStyle,
      this.height,
      this.width,
      this.axis,
      this.shapeHeight,
      this.shapeWidth,
      this.crossAxisCount});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    Widget dispColorContainer;

    /// If the [height] argument is null, the height will use the [DefaultHeight]
    if (height == null) {
      height = MediaQuery.of(context).size.height / 6.0;
    } else {
      height = height;
    }
    /// If the [width] argument is null, the height will use the [DefaultWidth]
    if (width == null) {
      width = MediaQuery.of(context).size.width;
    } else {
      width = width;
    }
    /// If the [axis] argument is null, the height will use the [DefaultAxis]
    if (axis == null) {
      axis = Axis.horizontal;
    } else {
      axis = axis;
    }

    /// If the [crossAxisCount] argument is null, the height will use the [DefaultCrossaxisCount]
    if (crossAxisCount == null) {
      crossAxisCount = 8;
    } else {
      crossAxisCount = crossAxisCount;
    }
    /// If the [shapeHeight] argument is null, the height will use the [DefaultShapeHeight]
    if (shapeHeight == null) {
      shapeHeight = 28.0;
    } else {
      shapeHeight = shapeHeight;
    }
    /// If the [shapeWidth] argument is null, the height will use the [DefaultShapeWidth]
    if (shapeWidth == null) {
      shapeWidth = 28.0;
    } else {
      shapeWidth = shapeWidth;
    }

    /// Check colorPaletteStyle for display color Container
    switch (colorPaletteStyle) {
      case colorStyle.STYLE_NORMAL:
        dispColorContainer = gridListColorRound(context);
        break;

      case colorStyle.STYLE_GRID_SQUARE:
        dispColorContainer = gridListColorSquare(context);
        break;

      case colorStyle.STYLE_ROUND:
        dispColorContainer = listColorRound(context);
        break;

      case colorStyle.STYLE_SQUARE:
        dispColorContainer = listColorSquare(context);
        break;

      default:
        dispColorContainer = gridListColorRound(context);
        break;
    }

    return SingleChildScrollView(
      child: new Container(
        child: dispColorContainer,
      ),
    );
  }

  /// If the [colorPaletteStyle] argument is STYLE_GRID_SQUARE then execute this Widget
  Widget gridListColorSquare(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        elevation: 5.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
        child: Container(
            height: height,
            width: width,
            margin: EdgeInsets.only(top: 4),
            child: GridView.builder(
                shrinkWrap: true,
                itemCount: fullMaterialColors.length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: crossAxisCount),
                itemBuilder: (BuildContext context, int index) {
                  return Padding(
                    padding: const EdgeInsets.all(6.0),
                    child: InkWell(
                      onTap: () {
                        onColorChange(fullMaterialColors[index]);
                      },
                      child: Container(
                        child: Padding(
                          padding: const EdgeInsets.all(1.5),
                          child: Container(
                            decoration: BoxDecoration(
                                color: fullMaterialColors[index],
                                border: Border.all(color: Colors.black),
                                shape: BoxShape.rectangle),
                          ),
                        ),
                      ),
                    ),
                  );
                })),
      ),
    );
  }

  /// If the [colorPaletteStyle] argument is STYLE_NORMAL then execute this Widget
  Widget gridListColorRound(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        elevation: 5.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
        child: Container(
            height: height,
            width: width,
            margin: EdgeInsets.only(top: 4),
            child: GridView.builder(
                shrinkWrap: true,
                itemCount: fullMaterialColors.length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: crossAxisCount),
                itemBuilder: (BuildContext context, int index) {
                  return Padding(
                    padding: const EdgeInsets.all(6.0),
                    child: InkWell(
                      onTap: () {
                        onColorChange(fullMaterialColors[index]);
                      },
                      child: Container(
                        child: Padding(
                          padding: const EdgeInsets.all(1.5),
                          child: Container(
                            decoration: BoxDecoration(
                                color: fullMaterialColors[index],
                                border: Border.all(color: Colors.black),
                                shape: BoxShape.circle),
                          ),
                        ),
                      ),
                    ),
                  );
                })),
      ),
    );
  }

  /// If the [colorPaletteStyle] argument is STYLE_ROUND then execute this Widget
  Widget listColorRound(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        elevation: 5.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
        child: Container(
          alignment: AlignmentDirectional.topStart,
          child: Container(
            alignment: Alignment.topLeft,
            height: height,
            width: width,
            margin: EdgeInsets.only(top: 4),
            child: ListView.builder(
              itemCount: fullMaterialColors.length,
              scrollDirection: axis,
              itemBuilder: (context, index) {
                return Padding(
                  padding: const EdgeInsets.all(6.0),
                  child: InkWell(
                    onTap: () {
                      onColorChange(fullMaterialColors[index]);
                    },
                    child: Container(
                      height: shapeHeight,
                      width: shapeWidth,
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.black),
                          color: fullMaterialColors[index],
                          shape: BoxShape.circle),
                    ),
                  ),
                );
              },
            ),
          ),
        ),
      ),
    );
  }

  /// If the [colorPaletteStyle] argument is STYLE_SQUARE then execute this Widget
  Widget listColorSquare(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        elevation: 5.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
        child: Container(
          height: height,
          width: width,
          margin: EdgeInsets.only(top: 4),
          child: ListView.builder(
            itemCount: fullMaterialColors.length,
            scrollDirection: axis,
            itemBuilder: (context, index) {
              return Padding(
                padding: const EdgeInsets.all(6.0),
                child: InkWell(
                  onTap: () {
                    onColorChange(fullMaterialColors[index]);
                  },
                  child: Container(
                    height: shapeHeight,
                    width: shapeWidth,
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.black),
                      color: fullMaterialColors[index],
                    ),
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
