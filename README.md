# colorspalette

Material Color palette is a Flutter widget, that can be customizable.

By default, it's Material Colors.

You can also use colorspalette widget to display color in your app. Example, you can set the colorspalette in a dialog and display the selected color in a ListTile, for settings.

## Getting Started

# How to use it


This examples use a static color for 'selectedColor'

## Screenshot
<img title="Main color selection" src="https://gitlab.com/rain_infotech/color_pallete/-/raw/master/assets/images/img1.jpg" width="400" />

<img title="Main color selection" src="https://gitlab.com/rain_infotech/color_pallete/-/raw/master/assets/images/img2.jpg" width="400" />



### Add to your Flutter project

you just need to add colors_palette as a dependency in your pubspec.yaml file.

```yaml
colors_palette: ^0.0.1
```

### Import

```dart
import 'package:colors_palette/colorspalette.dart';
```
### Use it

```dart
import 'package:colors_palette/colorspalette.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:example/Controller/colorController.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Color Palette Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: ColorPaletteDemo(),
    );
  }
}

class ColorPaletteDemo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar:  AppBar(
          centerTitle: true,
          title:  new Text(
            "Color Palette",
            style: new TextStyle(
              fontSize: 20,
              color: Colors.white,
            ),
          ),
        ),
        body: new Container(
          child: GetBuilder(
              init: ColorController(),
              builder: (ColorController colorController) {
                return new Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      height: 150.0,
                      child: Card(
                        elevation: 5.0,
                        color: Colors.black,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        margin: EdgeInsets.all(8.0),
                        child: Center(
                          child: new Text(
                            "Hello, Welcome to Color Palette",
                            style: TextStyle(
                                color: colorController.selectedColor,
                                fontSize: 20.0),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: new ListView(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: new Text(
                              "Normal Style Color Palette",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 20.0,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          ColorsPalette(
                            height: MediaQuery.of(context).size.height / 6.0,
                            width: MediaQuery.of(context).size.width,
                            axis: Axis.vertical,
                            crossAxisCount: 7,
                            colorPaletteStyle: colorStyle.STYLE_NORMAL, //Change Color Palette STYLE_NORMAL
                            onColorChange: (color) {
                              colorController.updateColor(color);
                            },
                          ),
                          Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: new Text(
                              "Style Round Color Palette",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 20.0,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          ColorsPalette(
                            height: MediaQuery.of(context).size.height / 10.0,
                            colorPaletteStyle: colorStyle.STYLE_ROUND, //Change Color Palette STYLE_ROUND
                            shapeHeight: 28,
                            shapeWidth: 28,
                            axis: Axis.horizontal,
                            onColorChange: (color) {
                              colorController.updateColor(color);
                            },
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(
                                  flex: 1,
                                  child: Column(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: new Text(
                                          "Style Round Color Palette vertical",
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 20.0,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                      ColorsPalette(
                                        colorPaletteStyle:
                                            colorStyle.STYLE_ROUND,   //Change Color Palette STYLE_ROUND
                                        shapeHeight: 40.0,
                                        shapeWidth: 40.0,
                                        axis: Axis.vertical,  //Change Axis For Vertical Direction
                                        onColorChange: (color) {
                                          colorController.updateColor(color);
                                        },
                                      ),
                                    ],
                                  )),
                              Expanded(
                                flex: 1,
                                child: Column(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: new Text(
                                        "Style Square Color Palette vertical",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 20.0,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                    ColorsPalette(
                                      height:
                                          MediaQuery.of(context).size.height /
                                              6.0,
                                      width: MediaQuery.of(context).size.width,
                                      shapeHeight: 40.0,
                                      shapeWidth: 40.0,
                                      colorPaletteStyle:
                                          colorStyle.STYLE_SQUARE,  //Change Color Palette STYLE_SQUARE
                                      axis: Axis.vertical,  //Change Axis For Vertical Direction
                                      onColorChange: (color) {
                                        colorController.updateColor(color);
                                      },
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: new Text(
                              "Style Grid Square Color Palette",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 20.0,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          ColorsPalette(
                            height: MediaQuery.of(context).size.height / 6.0,
                            width: MediaQuery.of(context).size.width,
                            crossAxisCount: 8,
                            colorPaletteStyle: colorStyle.STYLE_GRID_SQUARE, //Change Color Palette STYLE_GRID_SQUARE
                            onColorChange: (color) {
                              colorController.updateColor(color);
                            },
                          ),
                          Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: new Text(
                              "Style Square Color Palette",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 20.0,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          ColorsPalette(
                            height: 60.0,
                            colorPaletteStyle: colorStyle.STYLE_SQUARE,  //Change Color Palette STYLE_SQUARE
                            shapeHeight: 40.0,
                            shapeWidth: 60.0,
                            onColorChange: (color) {
                              colorController.updateColor(color);
                            },
                          ),
                        ],
                      ),
                    )
                  ],
                );
              }),
        ),
      ),
    );
  }
}



```






This project is a starting point for a Dart
[package](https://flutter.dev/developing-packages/),
a library module containing code that can be shared easily across
multiple Flutter or Dart projects.

For help getting started with Flutter, view our 
[online documentation](https://flutter.dev/docs), which offers tutorials, 
samples, guidance on mobile development, and a full API reference.
